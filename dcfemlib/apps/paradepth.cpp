// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <ctime>
#include <dcfemlib.h>
using namespace DCFEMLib;

#include <fem.h>
#include <longoptions.h>
#include <electrodeconfig.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
using namespace MyMesh;
using namespace MyVec;

int main(int argc, char *argv[] ){

//  clock();
  setbuf(stdout, NULL);

  LongOptionsList lOpt;
  lOpt.setLastArg( "dat-file" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "dimension", required_argument, 'd', "Dimension [3]" );

  int option_char = 0, option_index;
  int dimension = 3;
  bool verbose = false;

  if ( argc < 2 ){
    lOpt.printHelp( argv[ 0 ] );
    exit( 0 );
  }
  string dataFileName = argv[ argc - 1 ];

  while ( ( option_char = getopt_long( argc, argv, "?h"
				       "d:" // dimension
				       , lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'v': verbose = true; break;
    case 'd': dimension = atoi(optarg); break;
    default : cerr << WHERE_AM_I << " undefined option: " << (char)option_char << endl;
    }
  }

  ElectrodeConfigVector data;
  data.load( dataFileName );

  double quasiInf = 1e5;
  RVector amq( data.size(), 0.0 ), anq( data.size(), 0.0 );
  RVector bmq( data.size(), 0.0 ), bnq( data.size(), 0.0 );

  for ( size_t i = 0; i < data.size(); i ++ ){
    //    cout << i << " " << data[i].a() << " " << data[i].b() << " " << data[i].m() << " " << data[i].n() << endl;
    for ( int j = 0; j < dimension -1; j ++ ) amq[ i ] += pow( data.posA( i )[ j ] - data.posM( i )[ j ], 2.0 );
    for ( int j = 0; j < dimension -1; j ++ ) anq[ i ] += pow( data.posA( i )[ j ] - data.posN( i )[ j ], 2.0 );
    for ( int j = 0; j < dimension -1; j ++ ) bmq[ i ] += pow( data.posB( i )[ j ] - data.posM( i )[ j ], 2.0 );
    for ( int j = 0; j < dimension -1; j ++ ) bnq[ i ] += pow( data.posB( i )[ j ] - data.posN( i )[ j ], 2.0 );

    if ( data[ i ].n() == -1 ) {
      bnq[ i ] = quasiInf;
      anq[ i ] = quasiInf;
    }
    if ( data[ i ].b() == -1 ) {
      bmq[ i ] = quasiInf;
      bnq[ i ] = quasiInf;
    }    //    cout << amq[i] << " " << anq[i] << " " <<  bmq[i]  << " " << bnq[i] << endl;
  }

  data.calcK( PLANE );
  //  data.save("test.dat", "a b m n r k", true);

  RVector scale( (2.0 * PI_ ) / data.k() );
  double depth = 0.1, sens = 0.0;
  double depthQ = 4 * depth* depth;

  RVector tmp( data.size() );
  //  data.allK().save( "k.vec", Ascii );
  //  cout << data.posA( 0 ) << data.posB( 0 )<<data.posM( 0 )<<data.posN( 0 )<<endl;
  //cout << amq[0] << " " << anq[0] << " " << bmq[0] << " "<<bnq[0] << endl;

  while ( sens < 0.95 ){
//    tmp = 1.0 / sqrt( amq + depthQ ) - 1.0 / sqrt( anq + depthQ ) -
//    1.0 / sqrt( bmq + depthQ ) + 1.0 / sqrt( bnq + depthQ );
    tmp = 1.0 / sqrt( bnq + depthQ ) - 1.0 / sqrt( bmq + depthQ ) -
    1.0 / sqrt( anq + depthQ ) + 1.0 / sqrt( amq + depthQ );
    //tmp.save( "tmp.vec", Ascii);

    sens = sum ( 1.0 - tmp / scale ) / data.size();
    //    cout << sens << endl;
    depth = depth * 1.1;
    depthQ = 4 * depth * depth;
  }

  cout << depth << endl;
  return 0;
}

/*
$Log: paradepth.cpp,v $
Revision 1.11  2010/06/22 14:42:47  thomas
*** empty log message ***

Revision 1.10  2010/06/18 10:17:59  carsten
win32 compatibility commit

Revision 1.9  2005/07/13 13:36:49  carsten
*** empty log message ***

Revision 1.8  2005/07/13 12:27:12  carsten
*** empty log message ***

Revision 1.7  2005/07/13 12:12:04  carsten
*** empty log message ***

Revision 1.6  2005/07/13 11:45:23  thomas
paradepth

Revision 1.4  2005/06/30 17:32:16  carsten
*** empty log message ***

Revision 1.3  2005/06/01 18:26:51  carsten
*** empty log message ***

Revision 1.2  2005/05/03 15:55:34  carsten
*** empty log message ***

Revision 1.1  2005/03/29 16:57:46  carsten
add inversion tools

*/
