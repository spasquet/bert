// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef TAUCSWRAPPER__H
#define TAUCSWRAPPER__H

#include "myvec/matrix.h"
#include "myvec/vector.h"

#include "solver.h"


class TaucsWrapper : public SolverWrapper{
public:
  TaucsWrapper( ) : SolverWrapper(){}
  TaucsWrapper( const RDirectMatrix & S, bool verbose = false );
  TaucsWrapper( const RDirectMatrix & S, const string & ordertype, bool verbose = false );
  TaucsWrapper( const RDirectMatrix & S, const string & ordertype, double dropTol, double tolerance, bool verbose = false );
  ~TaucsWrapper();
 
  int factorise( );
  int preordering( );
  int solve( const RVector & rhs, RVector & solution );

protected:
  int initialize_( const RDirectMatrix & S );

  void * A_;
  void * AP_;
  void * L_;
  void * b_, * x_, * bP_, * xP_;
  
  string preorderStyle_;
  string logfileName_;
  bool preordering_;
  int * perm_, * invperm_;

};

int solveWithTaucsTest( const RDirectMatrix & S, RVector & x, const RVector & b, 
		      double tol, int maxiter, bool verbose = true );

int solveWithTaucsICCG( RDirectMatrix & S, const string & filebody, const vector < int > & vecRhs, 
			double dropTol, double tol, int maxiter, bool verbose );

int solveWithTaucsDirect( const RDirectMatrix & S, RVector & x, const RVector & b, 
		      double tol, int maxiter, bool verbose = true );

int solveWithTaucsDirect( const RDirectMatrix & S, vector < RVector > & x, const vector < RVector > & b, 
		      double tol, int maxiter, bool verbose = true );

#endif // TAUCSWRAPPER__H

/*
$Log: taucswrapper.h,v $
Revision 1.8  2007/03/07 16:38:14  carsten
*** empty log message ***

Revision 1.7  2005/10/12 17:34:39  carsten
*** empty log message ***

Revision 1.6  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.5  2005/02/22 21:51:43  carsten
*** empty log message ***

Revision 1.4  2005/02/14 18:58:54  carsten
*** empty log message ***

Revision 1.3  2005/01/11 19:12:26  carsten
*** empty log message ***

Revision 1.2  2005/01/09 19:13:55  carsten
*** empty log message ***

Revision 1.1  2005/01/06 20:28:30  carsten
*** empty log message ***


*/
