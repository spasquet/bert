// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "inversionstuff.h"

int savePotMatrix( const string & fname, const vector < RVector > & potMatrix ){

  FILE *file;
  file = fopen( fname.c_str(), "w+b" );
  int nPots = potMatrix.size();
  fwrite( &nPots, sizeof(int), 1, file );
  int size = potMatrix[ 0 ].size();
  fwrite( &size, sizeof(int), 1, file );

  for ( int i = 0; i < nPots; i ++ ){
    for ( int j = 0; j < size; j ++ ){
      fwrite( &potMatrix[ i ][ j ], sizeof(double), 1, file );
    }
  }
  fclose( file );
    return 1;
    
}

int loadPotMatrix( const string & fname, vector < RVector > & potMatrix ){
  potMatrix.clear();
  FILE *file;
  file = fopen( fname.c_str(), "r+b" );
  if ( file == NULL ) return 0;

  int nPots = 0;
  fread( &nPots, sizeof(int), 1, file );
  int size = 0;
  fread( &size, sizeof(int), 1, file );

  for ( int i = 0; i < nPots; i ++ ){
    potMatrix.push_back( RVector( (size_t)size ) );
    for ( int j = 0; j < size; j ++ ){
      fread( (char*)&potMatrix[ i ][ j ], sizeof( double ), 1, file );
    }
  }
  fclose( file );
  return 1;
}

double chiQuad( const RVector & soll, const RVector & ist, const RVector & err, bool isLog ){
  double chiq = 0.0;

//     function chiq=chi2(soll,ist,err,islog) 
// % CHI2 - computation of chi^2-error 
// % chiq = chi2(data,response,error[,islog]) 
// % data, response .. vectors to compare 
// % error .. error/standard deviation 
// % islog .. treat data and response as logarithmic (default=1) 
// if nargin<4, islog=1; end 
// if islog, chiq=mean(((log(ist)-log(soll))./log(1+err)).^2); 
// else, chiq=mean(((ist./soll-1).^2)./(err.^2)); end
  RVector tmp( soll.size() );

  if ( isLog ){
    tmp = ( ( log( ist ) - log( soll ) ) / log( err + 1.0) );
    chiq = mean( tmp * tmp ); 
  } else {
    chiq = mean( ( ( ist / soll -1.0 ) * ( ist / soll -1.0 ) ) / ( err * err ) );
  }
  return chiq;
}

double linesearch( const RVector & dataInput, const RVector & oldRhoa, const RVector & newRhoa, 
		   const RVector & err, int iter ){

  if ( dataInput.size() != oldRhoa.size() ) {
    cerr << "dimensions differ!" << endl; return 0.0;
  }
  if ( dataInput.size() != newRhoa.size() ) {
    cerr << "dimensions differ!" << endl; return 0.0;
  }
  
  RVector appRhoa;
  RVector chi2Vec( 101 );
  double tau = 0.0, mintau = 0.0;

  double minchi2 = chiQuad( dataInput, oldRhoa, err );
  chi2Vec[ 0 ] = minchi2;
 
  for ( int i = 1 ; i < chi2Vec.size() ; i++ ){
    tau = i * 1./(chi2Vec.size() -1 );
    //    appRhoa =  oldRhoa + ( newRhoa - oldRhoa ) * tau;
    appRhoa =  oldRhoa * pow( newRhoa / oldRhoa , tau ); //logarithmic interpolation

    chi2Vec[ i ] = chiQuad( dataInput, appRhoa, err );
    if ( chi2Vec[ i ] < minchi2 ){
      mintau = tau;
      minchi2 = chi2Vec[ i ];
    }
  }

  if ( iter > 0 ){
    save( chi2Vec, "Linesearch." + toStr( iter ) + ".vector" );
  }

  return mintau;
}
// Das ist die Version aus inversion.cpp
// double linesearch( const RVector & dataInput, const RVector & oldRhoa, const RVector & newRhoa, const RVector & err, int iter){

//   if ( dataInput.size() != oldRhoa.size() ) cerr << "dimensions differ!" << endl;
//   if ( dataInput.size() != newRhoa.size() ) cerr << "dimensions differ!" << endl;
  
//   RVector appRhoa;
//   //  RVector rmsVec( 101 );
//   RVector chi2Vec( 101 );
//   double tau = 0.0, mintau = 0.0;

//   double minchi2 = chiQuad( dataInput, oldRhoa, err );
//   chi2Vec[ 0 ] = minchi2;
// //   double minrms = rrms( dataInput, oldRhoa );
// //   rmsVec[ 0 ] = minrms;
 
//   for ( int i = 1 ; i < chi2Vec.size() ; i++ ){
//     tau = i * 1./(chi2Vec.size() -1 );
// //    appRhoa =  oldRhoa + ( newRhoa - oldRhoa ) * tau;
//     appRhoa =  oldRhoa * pow( newRhoa / oldRhoa , tau ); //logarithmic interpolation
//     //    rmsVec[ i ] = rrms( dataInput, appRhoa );
//     chi2Vec[ i ] = chiQuad( dataInput, appRhoa, err );
//     if ( chi2Vec[ i ] < minchi2 ){
//       mintau = tau;
//       minchi2 = chi2Vec[ i ];
//     }
//   }
//   char strdummy[128];
//   sprintf( strdummy, "Linesearch%d.rms.vector", iter ); save( chi2Vec, strdummy );

//   return mintau;
// }


/*
$Log: inversionstuff.cpp,v $
Revision 1.6  2006/10/06 16:53:39  carsten
*** empty log message ***

Revision 1.5  2006/05/08 16:25:59  carsten
*** empty log message ***

Revision 1.4  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.3  2005/04/12 11:41:09  carsten
*** empty log message ***

Revision 1.2  2005/01/13 20:10:42  carsten
*** empty log message ***

Revision 1.1  2005/01/13 16:27:34  carsten
*** empty log message ***

*/
