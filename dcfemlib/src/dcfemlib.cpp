// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include "dcfemlib.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

using namespace std;

#if defined ( __APPLE__ ) || ( defined (__SVR4) && defined (__sun) )
  int isinf(double x) { return !isfinite(x) && x==x; }
#endif

#ifdef MINGW
#include <windows.h>

BOOL APIENTRY DllMain (HINSTANCE hInst     /* Library instance handle. */ ,
                       DWORD reason        /* Reason this function is being called. */ ,
                       LPVOID reserved     /* Not used. */ ){
    switch (reason)    {
      case DLL_PROCESS_ATTACH:
        break;
      case DLL_PROCESS_DETACH:
        break;
      case DLL_THREAD_ATTACH:
        break;
      case DLL_THREAD_DETACH:
        break;
    }
    /* Returns TRUE on success, FALSE on failure */
    return TRUE;
}
#endif

namespace DCFEMLib{

string versionStr(){
  string vers( (string)(PACKAGE_NAME) + "-" + PACKAGE_VERSION + ", bugs and suggestions to: " + PACKAGE_AUTHORS);
  return vers;
}

int openFile(const string & fname, fstream * file,
             ios_base::openmode farg, bool verbose){

  file->open( fname.c_str(), farg );
  if ( !*file ){
    if ( verbose ){
         cerr << WHERE_AM_I << " " << fname << ": " << strerror( errno ) << " " << errno << endl;
    }
//     if ( termination ) {
//       cerr << "Termination " << termination << endl;
//       exit( 0 );
//     }
    return 0;
  }
  return 1;
}
int openInFile( const string & fname, fstream * file, bool verbose ){
  return openFile( fname, file, ios::in, verbose);
}
int openOutFile( const string & fname, fstream * file, bool verbose ){
  return openFile( fname, file, ios::out, verbose);
}

int countStringColumns( const string & str ){
  istringstream is( str );
  int columnCount = 0;
  string tmp;
  while ( is >> tmp ) columnCount++;
  return columnCount;
}

vector < string > getNonEmptyRow( fstream & file, char comment ){
  vector < string > row;
  while( (row = getRowSubstrings( file, comment ) ).size() == 0 && !file.eof() );
  return row;
}

vector < string > getRowSubstrings( fstream & file, char comment ){
  vector < string > subStrings;
  string str, tmp;
  getline( file, str );

  istringstream is( str.substr( 0, str.find( comment ) ) );
  while ( is >> tmp ) subStrings.push_back( tmp );
  return subStrings;
}

vector < string > getSubstrings( const string & sumString ){
  vector < string > subStrings;
  istringstream is( sumString );
  string tmp;
  while ( is >> tmp ) subStrings.push_back( tmp );
  return subStrings;
}

int findDomainDimension( const string & fileName ){
  int dimension = 0;

  fstream file; openInFile( fileName, &file );
  vector < string > row( getNonEmptyRow( file ) );

  if ( row.size() == 4 ) { // tetgen/triangle poly-format;
    dimension = toInt( row[ 1 ] );
  } else {
    cerr << WHERE_AM_I << " cannot determine dimension of: " << fileName << endl;
    return -1;
  }

  if ( dimension == 2 || dimension == 3 ){
    return dimension;
  } else {
    cerr << WHERE_AM_I << " frong dimension found: " << dimension << endl;
    return -1;
  }
  file.close();
  return -1;
}

int countColumns( const string & fname ){
  fstream file; if ( !openInFile( fname, & file ) ) {  return 0; }
  string str; getline( file, str );
  int columnCount = countStringColumns( str );
  file.close();
  return columnCount;
}

int countColumns( fstream & file ){
  int oldpos = file.tellg();
  //  cout << "oldPos" << oldpos << endl;
  string str;
  int count = 0;
  while( str.size() == 0 && count < 1e6 ){
    getline( file, str );
    count ++;
  }
  if ( count == 1e6 ) return -1;
  int columnCount = countStringColumns( str );
  file.seekg( oldpos );
  return columnCount;
}

std::map < float, float > loadFloatMap( const std::string & filename ){
    std::map < float, float > aMap;

    std::fstream file; if ( !openInFile( filename, & file ) ){
        std::cerr << WHERE_AM_I << " Map not found: " << filename << std::endl;
        return aMap;
    }

    std::vector < std::string > row;
    while ( ! file.eof() ) {
        row = getNonEmptyRow( file );
        if ( row.size() == 2 ) aMap[ toFloat( row[ 0 ] ) ] = toFloat( row[ 1 ] );
    }

    file.close();
    return aMap;
}

bool compare( double expect, double found, double tol, bool verbose ){
  if ( verbose && fabs( expect - found ) < tol ) cout << "...ok, ";
  else if ( verbose ) {
    cout << expect << " != " << found <<  " test ...fail, ";
  }
  return ( fabs( expect - found ) < tol );
}

} // namespace DCFEMLib

/*
$Log: dcfemlib.cpp,v $
Revision 1.23  2010/10/28 12:01:28  carsten
fix version conflict

Revision 1.22  2010/06/14 11:53:55  carsten
*** empty log message ***

Revision 1.21  2008/10/05 13:25:00  carsten
*** empty log message ***

Revision 1.20  2008/05/19 14:52:28  carsten
Add cholmodwrapper, interpolate V.2 in createSurface

Revision 1.19  2006/08/02 18:51:13  carsten
*** empty log message ***

Revision 1.17  2006/07/18 14:08:25  carsten
*** empty log message ***

Revision 1.16  2006/04/17 20:58:03  carsten
*** empty log message ***

Revision 1.15  2006/01/22 21:02:06  carsten
*** empty log message ***

Revision 1.14  2005/10/12 14:41:56  carsten
*** empty log message ***

Revision 1.13  2005/07/28 19:43:43  carsten
*** empty log message ***

Revision 1.12  2005/06/23 20:15:32  carsten
*** empty log message ***

Revision 1.11  2005/03/17 18:17:13  carsten
*** empty log message ***

Revision 1.10  2005/03/15 18:11:36  carsten
*** empty log message ***

Revision 1.9  2005/02/14 17:40:08  carsten
*** empty log message ***

Revision 1.8  2005/02/07 13:33:13  carsten
*** empty log message ***

Revision 1.7  2005/01/17 13:44:43  carsten
*** empty log message ***

Revision 1.6  2005/01/14 14:27:26  carsten
*** empty log message ***

Revision 1.5  2005/01/06 14:16:14  carsten
*** empty log message ***

Revision 1.4  2005/01/06 13:20:07  carsten
*** empty log message ***

Revision 1.3  2005/01/05 15:01:11  carsten
*** empty log message ***

Revision 1.2  2005/01/04 19:22:30  carsten
add in cvs

Revision 1.1.1.1  2005/01/03 18:00:09  carsten

start cvs dcfemlib


*/
